#include <form.h>
#include <panel.h>
#include <string.h>
#include "include/visual.h"

// lines, columns, y coordinate, x coordinate
WINDOW *make_window(int l, int c, int y, int x)
{
	WINDOW *w = newwin(l, c, y, x);
	box(w, 0, 0);
	return w;
}

WINDOW *make_window_r(const WINDOW *w, int l, int c)
{
	WINDOW *w1;
	int beg_x, beg_y;
	int max_x, max_y;
	getmaxyx(w, max_y, max_x);
	getbegyx(w, beg_y, beg_x);

	w1 = make_window(l, c, beg_y, max_x+1);
	return w1;
}

WINDOW *make_window_in(WINDOW *w, _Bool title)
{
	WINDOW *w1;
	int beg_x, beg_y;
	int max_x, max_y;
	getmaxyx(w, max_y, max_x);
	getbegyx(w, beg_y, beg_x);

	int lines_new = title ? max_y - beg_y - 2 - TITLE_HEIGHT: max_y - beg_y - 2;
	int cols_new = max_x - beg_x - 4;
	int y_new = title ? TITLE_HEIGHT + 1 : 1;
	int x_new = 2;

	w1 = derwin(w, lines_new, cols_new, y_new, x_new);
	wborder(w1, '|', '|', ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);

	return w1;
}

void title_bar(WINDOW *w, char *title, int align, int color)
{
	int beg_x, beg_y;
	int max_x, max_y;

	getbegyx(w, beg_y, beg_x);
	getmaxyx(w, max_y, max_x);

	int line_y = TITLE_HEIGHT;
	int line_beg_x = 1;
	int line_end_x = max_x - 2;
	int rtee_x = max_x - 1;
	int text_y = 1;
	int text_x;

	switch(align)
	{
		case -1:
			text_x = 3;
			break;
		case 0:
			text_x = (max_x - strlen(title)) / 2;
			break;
		default:
			text_x = max_x - strlen(title) - 2;
	}

	mvwaddch(w, line_y, 0, ACS_LTEE);
	mvwhline(w, line_y, line_beg_x, ACS_HLINE, line_end_x);
	mvwaddch(w, line_y, rtee_x, ACS_RTEE);

	wattron(w, A_BOLD | COLOR_PAIR(color));
	mvwprintw(w, text_y, text_x, title);
	wattrset(w, 0);
}

void info_text(WINDOW *w, char *text)
{
	int max_y, max_x;
	getmaxyx(w, max_y, max_x);
	mvwprintw(w, max_y-2, 2, "Info: %s", text);
	update_panels();
}

void construct_ask_creds(FORM **f, FIELD **fi, WINDOW **w, PANEL **p)
{
	int rows_out, cols_out;
	int rows_in, cols_in;
	int field_length = FIELD_BUFSIZE, field_height = 1;
	int i, j;

	for(i=0, j=0; i<Auth_Field_Count; ++i, j+=2)
	{
		fi[i] = new_field(field_height, field_length, 6+j, 11, 0, 0);
		set_field_back(fi[i], A_UNDERLINE);
		field_opts_off(fi[i], O_AUTOSKIP);
		set_max_field(fi[i], field_length);
		set_field_fore(fi[i], A_BOLD | COLOR_PAIR(RED));
	}
	fi[Auth_Field_Count] = NULL;

	*f = new_form(fi);

	scale_form(*f, &rows_in, &cols_in);
	rows_out = rows_in + 10;
	cols_out = cols_in + 40;

	*w = make_window(rows_out, cols_out, (LINES-rows_out)/2, (COLS-cols_out)/2);
	*p = new_panel(*w);
	keypad(*w, TRUE);

	WINDOW *sub = derwin(*w, rows_in, cols_in, (rows_in-6)/2, cols_in/2);
	set_form_win(*f, *w);
	set_form_sub(*f, sub);

	post_form(*f);

	title_bar(*w, "Authentication", POS_CENTER, RED);
	info_text(*w, "Press Enter to submit.");

	char *field_names[] = {
		"URL",
		"Bind DN",
		"Password",
		"Base DN"
	};

	for(i=0, j=0; i<Auth_Field_Count; ++i, j+=2)
		mvwprintw(sub, 6+j, 1, "%s: ", field_names[i]);
}

void show_one_panel(PANEL **all, short to_show)
{
	for(int i=0; i<Panel_Count; ++i)
	{
		if(i == to_show)
			show_panel(all[i]);
		else
			hide_panel(all[i]);
	}

	update_panels();
	doupdate();
}

void show_panels_except_one(PANEL **all, short to_hide)
{
	for(int i=0; i<Panel_Count; ++i)
	{
		if(i == to_hide)
			hide_panel(all[i]);
		else
			show_panel(all[i]);
	}

	update_panels();
	doupdate();
}

//TODO: make return int
void interact_with_conn(FORM **auth)
{
	int ch;												// Storage for keypresses
	WINDOW *win = form_win(*auth);						// Get the pointer to main form window
	form_driver(*auth, REQ_FIRST_FIELD);				// Move the cursor to the first field
	curs_set(1);										// Show the cursor while gathering data

	while((ch = wgetch(win)) != 10)						// Break out when ENTER is pressed
	{
		switch(ch)
		{
			case KEY_DOWN:								// On down arrrow key press
				form_driver(*auth, REQ_NEXT_FIELD);		// Move to the next field
				form_driver(*auth, REQ_END_LINE);		// End the current line
				break;
			case KEY_UP:
				form_driver(*auth, REQ_PREV_FIELD);     // Move to the previous field
				form_driver(*auth, REQ_END_LINE);		// End the current line
				break;
			case KEY_LEFT:
				form_driver(*auth, REQ_PREV_CHAR);     	// Move to the previous char
				break;
			case KEY_RIGHT:
				form_driver(*auth, REQ_NEXT_CHAR);     	// Move to the next char
				break;
			case KEY_DC:
				form_driver(*auth, REQ_DEL_CHAR);      	// Delete char under cursor
				break;
			case KEY_HOME:
				form_driver(*auth, REQ_BEG_LINE);      	// Go to the beginning
				break;
			case KEY_END:
				form_driver(*auth, REQ_END_LINE);      	// Go to the end
				break;
			case 127:									// Backspace
				form_driver(*auth, REQ_DEL_PREV);     	// Move to the previous field
				break;
			case 9:										// TAB key
				form_driver(*auth, REQ_NEXT_FIELD);		// Move to the next field
				form_driver(*auth, REQ_END_LINE);		// End the current line
				break;
			default:
//				if(field_index(current_field(*auth)) == PASSWORD_FIELD)   //TODO: Mask password, call function to modify field buffer
				form_driver(*auth, ch);					// Print the character in the current field
				break;
		}
	}
	form_driver(*auth, REQ_END_LINE);
	curs_set(0);										// Make the cursor invisible again
}

void fill_result_window(WINDOW *w, const char *message, _Bool error)
{
	char title[10] = "Success!";
	chtype color = BLUE;
	if(error)
	{
		color = RED;
		strcpy(title, "Error!");
	}

	title_bar(w, "Result", POS_CENTER, color);
	mvwprintw(w, TITLE_HEIGHT+5, 10, message);
	update_panels();
}

int extract_field_data(const FIELD *f, char *container)
{
	char *field_buf = field_buffer(f, 0);
	int i, j;

	for(i=0, j=0; i<FIELD_BUFSIZE; ++i)
	{
		if(field_buf[i] == ' ')
			continue;
		container[j] = field_buf[i];
		++j;
	}
	container[j] = '\0';

	return j;
}
