CC=gcc-8
LDLIBS=-lncurses -lpanel -lform -lldap
CPPFLAGS=-Wall -Wno-deprecated-declarations
APP=ldbr
SRC={ldbr,visual}.c

all:
	$(CC) $(SRC) $(LDLIBS) $(CPPFLAGS) -o $(APP)
debug:
	$(CC) $(SRC) $(LDLIBS) $(CPPFLAGS) -o $(APP) -g
clean:
	rm -rf ldbrr ldbr.dSYM

.PHONY: all debug
