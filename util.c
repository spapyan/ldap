#include <panel.h>
#include <stdlib.h>
#include <string.h>
#include "include/util.h"

char* ldif_read_one(FILE *f)
{
	char c;
	char *entry = malloc(sizeof(char) * ENTRY_BUFSIZE);
	entry[0] = '\0';
	int i = 0;


	while((c = fgetc(f)) != EOF)
	{
		// Discart blank lines at the beginning
		if(entry[0] == '\0' && c == ' ')
			continue;

		// If reached buffer limit
		if(i == ENTRY_BUFSIZE-1)
		{
			fprintf(stderr, "Reached buffer limit!\nENTRY_BUFSIZE: %d",
					ENTRY_BUFSIZE);
			return NULL;
		}

		// An empty line means the end of entry
		if(c == '\n' && entry[i-1] == '\n') 
		{
			entry[i-1] = '\0';
			break;
		}

		entry[i] = c;
		++i;
	}

	if(entry[0] == '\0')
		entry = NULL;

	return entry;
}

int ldif_get_attr(const char *entry, const char *key, char **to)
{
	char buffer[128];
	_Bool found_key = FALSE;
	int i = 0;
	int j = 0;

	// Find the needed key
	while(TRUE)
	{
		j = 0;

		// Read the key
		while(entry[i] != ':')
		{
			buffer[j] = entry[i];
			++i;
			++j; // Move from : to the whitespace
		}
		buffer[j] = '\0';
		++i;

		// Check if this is the needed key
		if(!strcmp(buffer, key))
		{
			found_key = TRUE;
			break;
		}

		// Jump over the unneded value
		while(entry[i] != '\n')
		{
			++i;
			continue;
		}
		++i;
	}

	if(!found_key)
		return FAILURE;

	j = 0;
	// Write the value to "to"
	while(entry[++i] != '\n')
	{
		buffer[j] = entry[i];
		++j;
	}
	buffer[j] = '\0';

	*to = malloc(sizeof(char)*strlen(buffer));
	strcpy(*to, buffer);

	return SUCCESS;
}
