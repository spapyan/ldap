#ifndef UTIL_H
#define UTIL_H

char* ldif_read_one(FILE *f);
int ldif_get_attr(const char *entry, const char *key, char **to);

#endif
