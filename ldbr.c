#include <panel.h>
#include <form.h>
#include <ldap.h>
#include "include/visual.h"

int auth_ldap(LDAP **conn, const char *url, const char *bidn, const char *pw);
int search_ldap(LDAP **conn, const char *badn, TreeElement *t);

int main()
{
	// Tree and content window pointers
	WINDOW *tree_out_w, *tree_in_w, *content_w;
	PANEL *tree_out_p, *tree_in_p, *content_p;

	// Connection dialog pointers
	FORM *simple_auth_form;
	FIELD *fields[Auth_Field_Count+1];
	WINDOW *simple_auth_win, *auth_result_win;
	PANEL *simple_auth_panel, *auth_result_panel;

	// LDAP
	LDAP *conn;
	char auth_data[Auth_Field_Count][FIELD_BUFSIZE];

	int ch, err;
//	FILE *data = fopen("data.txt", "r");
	int selection = 0;
	int e_count = 0;

	// Temporary data source and container
	TreeElement elems[1024];

	/************** read from file ***************/
//	while((entry = ldif_read_one(data)))
//	{
//		ldif_get_attr(entry, "dn", &elems[e_count].dn);
//		ldif_get_attr(entry, "parent", &elems[e_count].parent);
//		++e_count;
//	}

	// Initialize curses
	initscr();
	start_color();
	cbreak();									// Disable buffering
	noecho();									// Don't echo the keypresses
	keypad(stdscr, TRUE);						// Enable special keys like Arrow, F1
	curs_set(0);								// Hide the cursor

	init_pair(BLUE, COLOR_BLUE, COLOR_BLACK);
	init_pair(RED, COLOR_RED, COLOR_BLACK);

	/******** Tree window vars ********/
	int tree_lines = LINES;						// 100% of the screen height
	int tree_cols = COLS/3;						// 25% of the screen width
	int tree_y = 0;								// Left upper edge
	int tree_x = 0;								// Left upper edge

	/******** Content window vars ********/
	int content_lines = LINES;  				// 100% of the screen height
	int content_cols = COLS - tree_cols - 1;    // The rest of the screen width

	/******** Auth result window vars ********/
	int result_cols, result_lines;
	int result_y, result_x;

	/******** WINDOWs ********/
	tree_out_w = make_window(tree_lines, tree_cols, tree_y, tree_x);	// Create the outer tree window
	tree_in_w = make_window_in(tree_out_w, TRUE);						// Create the inner tree window
	content_w = make_window_r(tree_out_w, content_lines, content_cols);	// Create the content window

	/******** PANELs ********/
	tree_out_p = new_panel(tree_out_w);									// Attach the outer tree window to its panel
	tree_in_p = new_panel(tree_in_w);									// Attach the inner tree window to its panel
	content_p = new_panel(content_w);									// Attach the content window to its panel

	/******** Title bars ********/
	title_bar(tree_out_w, "Directory Tree", POS_LEFT, BLUE);			// Tree title bar
	title_bar(content_w, "Content", POS_CENTER, BLUE);					// Content title bar

	/******** Connection dialog ********/
	construct_ask_creds(&simple_auth_form, fields, &simple_auth_win,	// Construct the simple auth window
			&simple_auth_panel);

	/******* Authentication result dialog *******/
	getmaxyx(simple_auth_win, result_lines, result_cols);
	getbegyx(simple_auth_win, result_y, result_x);
	auth_result_win = make_window(result_lines, result_cols,
			result_y, result_x);
	auth_result_panel = new_panel(auth_result_win);

	/* Put all the panels into an array */
	PANEL *all_panels[] = {
		tree_out_p,
		tree_in_p,
		content_p,
		simple_auth_panel,
		auth_result_panel
	};

	show_one_panel(all_panels, Panel_Simple_Auth);						// Show the connection details window only
	interact_with_conn(&simple_auth_form);								// Interact with the connection form

	/* Extract the form data */
	for(int i=0; i<Auth_Field_Count; ++i)
		extract_field_data(fields[i], auth_data[i]);

	/****** Authentication against LDAP server ******/
	while((err = auth_ldap(&conn, auth_data[URL_Field], auth_data[BIDN_Field],
					auth_data[Password_Field])) != LDAP_SUCCESS)
	{
		fill_result_window(auth_result_win, ldap_err2string(err), TRUE);
		show_one_panel(all_panels, Panel_Auth_Result);
		ch = wgetch(auth_result_win);
		show_one_panel(all_panels, Panel_Simple_Auth);
		interact_with_conn(&simple_auth_form);
		/* Extract the form data */
		for(int i=0; i<Auth_Field_Count; ++i)
			extract_field_data(fields[i], auth_data[i]);
	}

	show_panels_except_one(all_panels, Panel_Simple_Auth);				// Hide the conn window, show everything else
    
    /** TEMP **/
    hide_panel(auth_result_panel);

	e_count = search_ldap(&conn, auth_data[BADN_Field], elems);					// Search the LDAP server

	/****** Populate the tree ******/
	keypad(tree_in_w, TRUE);
	while(TRUE)
	{
		wattron(tree_in_w, A_BOLD);
		for(int i=0; i<e_count; ++i)
		{
			if(i == selection)
				wattron(tree_in_w, A_REVERSE);
			mvwprintw(tree_in_w, i+1, 2, elems[i].dn);
			wattroff(tree_in_w, A_REVERSE);
			update_panels();
			doupdate();
		}
		wattroff(tree_in_w, A_BOLD);

		ch = wgetch(tree_in_w);

		switch (ch)
		{
			case KEY_DOWN:
				if(selection == e_count-1)
					selection = 0;
				else
					++selection;
				break;
			case KEY_UP:
				if(selection == 0)
					selection = e_count-1;
				else
					--selection;
				break;
			default:
				break;
		}
	}


	// Wait for input
	getch();

	// End curses mode
	endwin();

	return 0;
}

// TODO: Improve error reporting
int auth_ldap(LDAP **conn, const char *url, const char *bidn, const char *pw)
{
    int version = LDAP_VERSION3, err;

    // Initialize a connection
    if((err = ldap_initialize(conn, url)) != LDAP_SUCCESS)
		return err;
	
    // Set version to 3
    ldap_set_option(*conn, LDAP_OPT_PROTOCOL_VERSION, &version);

    // Do bind with simple auth
    if((err = ldap_simple_bind_s(*conn, bidn, pw)) != LDAP_SUCCESS)
		return err;

    return LDAP_SUCCESS;
}

// TODO: Improve error reporting
int search_ldap(LDAP **conn, const char *badn, TreeElement *t)
{
    LDAPMessage *response, *entry;
	int i, count, err;

	// Do search
    if((err = ldap_search_ext_s(*conn, badn, LDAP_SCOPE_SUBTREE, "(objectClass=*)",
				NULL, 0, NULL, NULL, NULL, 0, &response)) != LDAP_SUCCESS)
//		printw("Error: %s", ldap_err2string(err));
		return ERR_LDAP_SEARCH;

	// Count found entries
	count = ldap_count_entries(*conn, response);	

	for(i = 0, entry = ldap_first_entry(*conn, response);
			entry != NULL && i<1024;				/******** USE ELEM_SIZE HERE *********/
			++i, entry = ldap_next_entry(*conn, entry))
	{
		t[i].dn = ldap_get_dn(*conn, entry);
		t[i].parent = "fake";
	}
	return count;
}

