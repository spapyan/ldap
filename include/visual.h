#ifndef TREE_H
#define TREE_H

#define TITLE_HEIGHT 2

/*********************************************** Alignment **/
#define POS_CENTER 0
#define POS_LEFT -1
#define POS_RIGHT 1

/*********************************************** Colors *****/
#define BLUE 1
#define RED 2

/*********************************************** Fields *****/
#define FIELD_BUFSIZE 40
enum fields {
	URL_Field,
	BIDN_Field,
	Password_Field,
	BADN_Field,
	Auth_Field_Count
};

/*********************************************** Panels ****/
enum panels {
	Panel_Tree_Out,
	Panel_Tree_In,
	Panel_Content,
	Panel_Simple_Auth,
	Panel_Auth_Result,
	Panel_Count
};

/*********************************************** LDAP ******/
#define ERR_LDAP_SUCCESS 0
#define ERR_LDAP_INITIALIZE 1
#define ERR_LDAP_BIND 2
#define ERR_LDAP_SEARCH 3


/*********************************************** Entries ***/
#define ELEM_SIZE(e) (sizeof(e)/sizeof(e[0]))
#define ENTRY_BUFSIZE 1024
#define SUCCESS 1
#define FAILURE 0 

typedef struct tree_elem_s {
	char *dn;
	char *parent;
} TreeElement;

/*********************************************** FUNCTIONS */
WINDOW* make_window(int l, int c, int y, int x);
WINDOW *make_window_r(const WINDOW *w, int l, int c);
WINDOW *make_window_in(WINDOW *w, _Bool title);
void title_bar(WINDOW *w, char *title, int align, int color);
void info_text(WINDOW *w, char *text);
void construct_ask_creds(FORM **f, FIELD **fi, WINDOW **w, PANEL **p);
void show_one_panel(PANEL **all, short to_show);
void show_panels_except_one(PANEL **all, short to_hide);
void interact_with_conn(FORM **auth);
int extract_field_data(const FIELD *f, char *container);
void fill_result_window(WINDOW *w, const char *message, _Bool error);

#endif
