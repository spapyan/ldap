#include <ldap.h>
#include <stdio.h>

int auth_ldap()
{
    LDAP *conn;
    LDAPMessage *response, *entry;
    int version = LDAP_VERSION3;
    int err, count;

    // initialize
    err = ldap_initialize(&conn, "ldap://localhost:1389");
    fprintf(stderr, "Init: %s\n", ldap_err2string(err));

    // set version to 3
    ldap_set_option(conn, LDAP_OPT_PROTOCOL_VERSION, &version);

    // do bind with simple auth
    err = ldap_simple_bind_s(conn, "cn=admin,dc=papyan,dc=am", "barev123");
    fprintf(stderr, "Bind: %s\n", ldap_err2string(err));

    err = ldap_search_ext_s(conn, "dc=papyan,dc=am", LDAP_SCOPE_SUBTREE, "(objectClass=*)", NULL, 0, NULL, NULL, NULL, 0, &response);
    fprintf(stderr, "Search: %s\n", ldap_err2string(err));

	count = ldap_count_entries(conn, response);	
    fprintf(stderr, "Received entry count: %d\n", count);

	for(entry = ldap_first_entry(conn, response); entry != NULL; entry = ldap_next_entry(conn, entry))
	{
		fprintf(stderr, "Found DN: %s\n", ldap_get_dn(conn, entry));
	}


    err = ldap_unbind_s(conn);
    fprintf(stderr, "Unbind: %s\n", ldap_err2string(err));


    return 0;
}
